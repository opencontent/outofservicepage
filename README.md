## Build

    docker build -t some-content-nginx .    
    
## Run

    docker run --name some-nginx -d -p 8081:80 some-content-nginx
